#ifndef __CONVERT_H__
#define __CONVERT_H__

#include <string>
#include <iostream>
#include "TStack.h"
using namespace std;

bool Check_Symbols(string);
bool Check_Brackets(string);
string Postfix_Conversion(string);
int Operation_Number(char);
bool IsOperation(char);
TData Calculation(string);

#endif
